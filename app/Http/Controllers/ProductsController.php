<?php

namespace App\Http\Controllers;

use App\Models\Product;
use http\Exception;
use Illuminate\Http\Request;
use \App\Http\Requests\ProductsRequest;

class ProductsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param bool $status
     * @param string $message
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\request()->exists("message")){
            $message = \request()->get("message");
        }
        $resources = Product::orderBy('name')->paginate(10);
        return view("index", compact(["resources", "message"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {
        $status = true;
        try{

            Product::create($request->all());

        }catch(Exception $e){

            $message = "Falha ao cadastrar produto!";
            return redirect()->route('products.index', compact("message"));

        }
        $message = "Produto cadastrado com sucesso!";
        return redirect()->route('products.index', compact("message"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = true;
        try{

            $resource = Product::findOrFail($id);

        }catch(Exception $e){

            $status = false;
            $message = "Produto não encontrado";
            return redirect()->route('products.index', compact("message"));

        }

        return view("show", compact(["resource"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = true;
        try{

            $resource = Product::findOrFail($id);

        }catch(Exception $e){

            $status = false;
            $message = "Produto não encontrado";
            return redirect()->route('products.index', compact("message"));

        }

        return view("edit", compact(["resource"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductsRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductsRequest $request, $id)
    {
        $status = true;
        try{

            $resource = Product::find($id);
            $resource->update($request->all());

        }catch(Exception $e){

            $status = false;
            $message = "Falha ao atualizar produto!";
            return redirect()->route('products.index', compact("message"));

        }
        $message = "Produto atualizado com sucesso!";
        return redirect()->route('products.index', compact("message"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = true;
        try{

            $resource = Product::find($id);
            $resource->delete();

        }catch(Exception $e){

            $message = "Produto não encontrado";
            return redirect()->route('products.index', compact("message"));

        }
        $message = "Produto excluído com sucesso!";
        return redirect()->route('products.index', compact("message"));
    }
}
