@extends('layout')
@section('content')

    <a class="waves-effect waves-light btn-large right" href="{{route('products.create')}}"><i class="material-icons left">add_box</i>Cadastrar</a>
    <h4>Produtos</h4>
    <table class="highlight">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Preço</th>
            <th>Quantidade em Estoque</th>
            <th>Opções</th>
        </tr>
        </thead>
        <tbody>
            @foreach($resources as $resource)
                <tr>
                    <td>{{$resource->name}}</td>
                    <td>{{"R$" . number_format($resource->amount, 2)}}</td>
                    <td>{{$resource->quantity}}</td>
                    <td>
                        <a href="{{route('products.show', ['id' => $resource->id])}}" class="options"><i class="material-icons" title="Visualizar">info</i></a>
                        <a href="{{route('products.edit', ['id' => $resource->id])}}" class="options"><i class="material-icons" title="Editar">edit</i></a>
                        <form class="form-options" action="{{route('products.destroy', ['id' => $resource->id])}}" method="post">
                            {{csrf_field()}}
                            {{method_field('delete')}}
                            <button type="submit"><i class="material-icons left">delete</i></button>
                        </form>
                        </td>
                </tr>
                @endforeach
        </tbody>
    </table>
    {!! $resources->links('vendor.pagination.default') !!}
@endsection