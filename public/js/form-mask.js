$(document).ready(function(){
    //Máscara Valor Monetário
    $('#amount').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    //Somente letras e espaços
    $('#name').keypress(function (e) {
        var name = String.fromCharCode(e.keyCode);
        if(!(/^([a-zA-ZáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]*)$/.test(name))){
            e.preventDefault();
        }
    });
});
